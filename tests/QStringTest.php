<?php

/**
* Marian Munteanu @ 2020
* 2020-04-05 17:53 marian (marian)
* QStringTest.php
*/

use PHPUnit\Framework\TestCase;
use Qp\QString;

final class QStringTest extends TestCase {

   
    /**
     * [test_QStringCanBeCreated description]
     * @author Marian Munteanu
     * @date   2020-04-05T18:34:33+0200
     * @return [type]                   [description]
     * @testdox QString can be created
     */
    public function test_QStringCanBeCreated(): void {
        $this->assertInstanceOf(
            QString::class,
            new QString("QStringTest")
        );
    }

    public function test_StrignOperations(): void {
        $str = new QString("QStringTest");

        $this->assertNotEquals( $str , "QStringTest2");
        $this->assertEquals( $str , "QStringTest");


        $this->assertFalse( $str->contains( "" ) );
        $this->assertFalse( $str->contains("Cab") );
        $this->assertTrue( $str->contains("QStr") );
        $this->assertTrue( $str->contains("ingT") );
        $this->assertFalse( $str->contains("ingt") );
        $this->assertTrue( $str->contains("ingt", true) );

        $this->assertTrue( $str->contains( (new QString("qstr")) , true ) );
        $this->assertTrue( $str->contains( (new QString("QStr")) ) );
        $this->assertFalse( $str->contains( (new QString("")) ) );
        $this->assertFalse( $str->contains( (new QString("aba")) ) );

        $this->assertTrue( (new QString("0123"))->doarCifre() );
        $this->assertFalse( (new QString(" 0123"))->doarCifre() );
        $this->assertFalse( (new QString("01 23"))->doarCifre() );
        $this->assertFalse( (new QString(" "))->doarCifre() );
        $this->assertFalse( (new QString(""))->doarCifre() );

        $this->assertTrue( (new QString("abcABCC"))->doarLitere() );
        $this->assertFalse( (new QString(" abcABCC"))->doarLitere() );
        $this->assertFalse( (new QString("0123"))->doarLitere() );
        $this->assertFalse( (new QString(" "))->doarLitere() );
        $this->assertFalse( (new QString(""))->doarLitere() );
        $this->assertFalse( (new QString("a.b"))->doarLitere() );

        $this->assertEquals( $str->mid( 1 ) , "StringTest" );
        $this->assertEquals( $str->left( 2 ) , "QS" );

        $this->assertTrue( $str == "QStringTest" );
        $this->assertTrue( $str->startsWith( "QStri" ) );

        $this->assertFalse( $str->startsWith( "ABca" ) );
        $this->assertFalse( $str->startsWith( "" ) );
        $this->assertFalse( $str->mid( 2 ) == "tringTes" );
        $this->assertTrue( $str->left( 3 ) == "QSt" );
        $this->assertFalse( $str->left( 3 ) == "QStr" );

        $this->assertTrue( (new QString("123"))->toDouble() == 123 );
        $this->assertFalse( (new QString(" 1,23 "))->toDouble() == 123 );

        $this->assertTrue( (new QString("123"))->doarCifre() );
        $this->assertFalse( (new QString(" 123"))->doarCifre() );
        
        $this->assertTrue( (new QString("123"))->toInt() == 123 );
        $this->assertFalse( (new QString("0.123"))->toInt() == 0123 );

        $this->assertTrue( (new QString("1,2,3"))->split(',') === ['1','2','3'] );
        $this->assertTrue( (new QString("1,2,3"))->split('.') === ['1,2,3'] );
        $this->assertFalse( (new QString("1,2,3"))->split(',') === ['1','2'] );
    }
}
