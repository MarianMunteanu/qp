<?php

/**
* Marian Munteanu @ 2020
* 2020-04-02 00:43 marian (marian)
* QString.php
*/

namespace Qp;

class QString extends QObject {

    private $v;
    
    function __construct($str)
    {
        $this->v = $str.'';
        // if(is_string($str) || $str instanceof QString) $this->v = $str.'';
    }

    /**
     * [add description]
     * @author Marian Munteanu
     * @date   2020-04-02T01:05:04+0200
     * @param  [type]                   $str [description]
     */
    public function add($str){
        if(is_string($str)) $this->v .= $str;
        return $this;
    }

    public function contains($str, $ignoreCase = false): bool {
        $s1 = $this.'';
        $s2 = $str;
        if($ignoreCase){
            $s1 = strtolower($s1);
            $s2 = strtolower($s2);
        }
        return strlen($s2 = $s2.'')>0 && (is_string($s2) && is_string($s1) && (strpos($s1, $s2) !== FALSE) );
    }

    /**
     * [startsWith description]
     * @author Marian Munteanu
     * @date   2020-04-02T01:36:32+0200
     * @param  [type]                   $str        [description]
     * @param  boolean                  $ignoreCase [description]
     * @return [type]                               [description]
     */
    public function startsWith($str, $ignoreCase = false){
        $s1 = $this.'';
        $s2 = $str;
        if($ignoreCase){
            $s1 = strtolower($s1);
            $s2 = strtolower($s2);
        }
        return ( is_string($s2) && is_string($s1) && (strrpos($s1, $s2, -strlen($s1)) !== FALSE) );
    }

    /**
     * [endsWith description]
     * @author Marian Munteanu
     * @date   2020-04-02T01:19:05+0200
     * @param  [type]                   $str           [description]
     * @param  boolean                  $caseSensitive [description]
     * @return [type]                                  [description]
     */
    public function endsWith($str, $ignoreCase = false){
        $s1 = $this.'';
        $s2 = $str;
        if($ignoreCase){
            $s1 = strtolower($s1);
            $s2 = strtolower($s2);
        }
        return ( is_string($s2) && is_string($s1) && (($temp = strlen($s1) - strlen($s2)) >= 0 && strpos($s1, $s2, $temp) !== FALSE) );
    }

    /**
     * [split description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:24:13+0200
     * @param  [type]                   $d [description]
     * @param  [type]                   $s [description]
     */
    public function split( $d, $s = null ): array {
        $ts = $s ? $s : $this.'';
        return explode( $d , $ts);
    }

    /**
     * [splitLast description]
     * @author Marian Munteanu
     * @date   2020-04-18T20:32:39+0200
     * @param  string                   $sep [description]
     * @return [type]                        [description]
     */
    public function splitLast(string $sep): array {
        $ls = strrpos($this.'', $sep);
        $lp = trim(mb_substr($this.'', 0, $ls));
        $pp = trim(mb_substr($this.'', $ls));
        return [$pp, $lp];
    }

    /**
     * [trim description]
     * @author Marian Munteanu
     * @date   2020-04-02T01:54:06+0200
     * @return [type]                   [description]
     */
    public function trim(){
        $this->v = trim($this.'');
        return $this;
    }

    /**
     * [mid description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:00:25+0200
     * @param  [type]                   $s [description]
     * @param  [type]                   $l [description]
     * @return [type]                      [description]
     */
    public function mid( $s, $l = null): QString {
        $s = (int)$s;
        $l = (int)$l;
        $ret = $l>0 
            ? new QString( substr( $this, $s, $l ) ) 
            : new QString( substr( $this, $s ) ) ;
        return $ret;
    }

    /**
     * [left description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:03:17+0200
     * @param  [type]                   $n [description]
     * @return [type]                      [description]
     */
    public function left( $n ): QString {
        return $this->primeleNChars( $n );
    }

    /**
     * [indexOf description]
     * @author Marian Munteanu
     * @date   2020-04-06T00:53:34+0200
     * @param  [type]                   $str        [description]
     * @param  boolean                  $ignoreCase [description]
     * @return [type]                               [description]
     */
    public function indexOf($str, $ignoreCase = false): int {
        return strpos($this, $str);
    }

    /**
     * [primeleNChars description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:02:25+0200
     * @param  [type]                   $n [description]
     * @return [type]                      [description]
     */
    public function primeleNChars( $n ): QString {
        return new QString( substr( $this, 0, $n ) );
    }

    /**
     * [Now description]
     * @author Marian Munteanu
     * @date   2020-04-02T01:02:25+0200
     * @param  string                   $format [description]
     * @param  string                   $tz     [description]
     */
    public static function now($format = "Ymd_His", $tz = 'Europe/Bucharest'): string {
        // date_default_timezone_set($tz);
        // return date($format);
        return ( new \DateTime("now", new \DateTimeZone($tz)))->format($format);
    }

    /**
     * [toDouble description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:19:20+0200
     * @return [type]                   [description]
     */
    public function toDouble(): float {
        return (double)($this."");
    }

    /**
     * [toInt description]
     * @author Marian Munteanu
     * @date   2020-04-05T19:19:47+0200
     * @return [type]                   [description]
     */
    public function toInt(): int {
        return (int)($this."");
    }

    /**
     * [doarCifre description]
     * @author Marian Munteanu
     * @date   2020-04-05T18:55:17+0200
     * @return [type]                   [description]
     */
    public function doarCifre(){
        return $this->length()>0 && ( !preg_match('/[^0-9]/', $this) );
    }

    /**
     * [doarLitere description]
     * @author Marian Munteanu
     * @date   2020-04-05T18:56:08+0200
     * @return [type]                   [description]
     */
    public function doarLitere(){
        return $this->length()>0 && ( !preg_match('/[^A-Za-z]/', $this) );
    }

    /**
     * [toLowerCase description]
     * @author Marian Munteanu
     * @date   2020-04-12T03:24:48+0200
     * @return [type]                   [description]
     */
    public function toLowerCase(): QString {
        return new QString( strtolower( $this.'' ) );
    }


    function length(){
        return strlen($this);
    }
    function size(){
        return $this->length();
    }
    function toString(){
        return $this->v;
    }
    public function __toString(){
        return $this->toString();
    }
}
