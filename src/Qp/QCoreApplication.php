<?php

/**
* Marian Munteanu @ 2020
* 2020-04-01 01:43 marian (marian)
* QCoreApplication.php
*/

namespace Qp;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class QCoreApplication extends QObject {

    const ENV_TEST  = 'TEST';
    const ENV_DEV   = 'DEV';
    const ENV_UAT   = 'UAT';
    const ENV_PROD  = 'PROD';

    const DIR_APP_ROOT = "DIR_APP_ROOT";
    const DIR_WEB_ROOT = "DIR_WEB_ROOT";

    public $dbEngine = null;

    var $env = self::ENV_PROD;
    var $o = null;
    var $request = null;
    var $logArr = [];

    abstract protected function initApp();

    public function __construct($o) {
        $this->o = $o;
        if (isset($o) && isset($o['env']) && $o['env']) {
            $this->env = $o['env'];
        }
        $this->request = Request::createFromGlobals();
        $this->logArr = [
            'IP'            => $this->getIP(),
            'UserAgent'     => $_SERVER["HTTP_USER_AGENT"],
            'RequestUri'    => $_SERVER["REQUEST_URI"],
            'Referer'       => @$_SERVER["HTTP_REFERER"],
            'Tara'          => @$_SERVER["HTTP_X_APPENGINE_COUNTRY"],
            'Oras'          => @$_SERVER["HTTP_X_APPENGINE_CITY"],
        ];
        $this->initApp();
    }

    /**
     * [exec description]
     * @author Marian Munteanu
     * @date   2020-04-06T02:32:26+0200
     * @return [type]                   [description]
     */
    public function exec(): int {
        $path = $this->request->getPathInfo();
        $path = ($path == '/' || $path == '') ? '/index' : $path;
        $r_appName = ($t_arr = explode('/', $path)) && count($t_arr)>1 ? $t_arr[1] : '';

        $response = $this->get404();
        try {

            if( $r_appName && file_exists($af = $this->o[self::DIR_APP_ROOT] . '/apps/apps.php') ){
                $this->r_appName = $r_appName;
                $apps_p = include_once $af ;
                if( array_key_exists($r_appName, $apps_p) ){
                    $appName = ($t_n = $apps_p[$r_appName]) ? $t_n : $r_appName;
                    if(is_file( $f = $this->o[self::DIR_APP_ROOT] . '/apps/' . $appName . '/app.php' )){
                        require_once $f;
                        $className = 'apps\\' . $appName;
                        
                        if(class_exists($className)
                            && ($t_app = new $className($this))
                            && ($t_app instanceof \Qp\apps\QpAppInterface) ){
                            $response = $t_app->getResponse();
                        }
                    }
                }
            }

        } catch (\Exception $ex) {
            $response = new Response('An error occurred: ' . $ex->GetMessage(), 500);
        }
        if ($response) {
            $response->send();
        }
        return 0;
    }

    /**
     * [getAppName description]
     * @author Marian Munteanu
     * @date   2020-04-16T15:16:19+0200
     * @return [type]                   [description]
     */
    public function getAppName(): QString {
        return (new QString( $this->name() ))->trim();
    }

    public function isDev($o = null) {
        return isset($this->env) && $this->env == self::ENV_DEV;
    }
    public function isUat($o = null) {
        return isset($this->env) && $this->env == self::ENV_UAT;
    }
    public function isProd($o = null) {
        return isset($this->env) && $this->env == self::ENV_PROD;
    }

    /**
     * [get404 description]
     * @author Marian Munteanu
     * @date   2020-04-09T23:33:28+0200
     * @return [type]                   [description]
     */
    public function get404(): Response {
        $r404 = "404";
        if( file_exists($tf = $this->o[self::DIR_APP_ROOT] . '/apps/404.php') && ($ts = include $tf) ){
            $r404 = $ts ;
        }
        return new Response($r404, 404);
    }

    /**
     * [getIP description]
     * @author Marian Munteanu
     * @date   2020-04-01T01:51:54+0200
     * @return [type]                   [description]
     */
    public static function getIP() {
        return ( ( isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']) ) ? $_SERVER['HTTP_CLIENT_IP'] :
                ( (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] :
                $_SERVER['REMOTE_ADDR']
                )
                );
    }

}
