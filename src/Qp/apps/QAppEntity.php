<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 16:27 marian (marian)
* QpAppEntity.php
*/

namespace Qp\apps;

use \Qp\db\Entity;

class QAppEntity extends Entity {

    public function __construct(QApp $app, array $o = null) {
        parent::__construct($app->cApp());
        $this->app = $app;
        $this->o = $o;
    }

    /**
     * [getName description]
     * @author Marian Munteanu
     * @date   2020-04-16T15:26:06+0200
     * @return [type]                   [description]
     */
    public function getName(): string {
        $lname = $this->name();
        if($this->o && isset($this->o['name']) && $this->o['name']){
            $lname = $this->o['name'];
        }
        if($lname == 'QAppEntity'){
            throw new \Exception("QAppEntity not permitted as name", 1);
        }
        return $this->cApp->getAppName() . "_" . $this->app->getAppName() . "_" . $lname;
    }
}
