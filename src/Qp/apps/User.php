<?php

/**
* Marian Munteanu @ 2022
* 2022-12-18 00:44 marian (marian)
* User.php
*/

namespace Qp\apps;

/** @Entity **/
class User extends QAppEntity {
    const USERNAME = 'USERNAME';
    const EMAIL = 'EMAIL';
    const PASS = 'PASS';
    const LEVEL = 'LEVEL';
    const XAT = 'XAT';
    const FNAME = 'FNAME';
    const LNAME = 'LNAME';
    const AVATAR = 'X10_AVATAR';

    private $level = null;

    var $username = null;
    var $email = null;
    var $first_name = null;
    var $last_name = null;

    /**
     * [getLevel description]
     * @author Marian Munteanu
     * @date   2022-12-26T11:50:14+0200
     * @return [type]                   [description]
     */
    function getLevel(): int {
        if(null === $this->level){
            if( (($c_tok = $this->app->getClientSettings()[self::XAT] ?? null)
                    || ($c_tok = $this->app->hd_rq->get($this->app->tokenHeader()) ?? null))
                && ($lid = $this->load(0, [self::XAT . ' =' => $c_tok])) > 0
                && isset($this->set[self::LEVEL])
            ) {
                $this->level = $this->set[self::LEVEL];
                $this->username = $this->set[self::USERNAME];
                $this->email = $this->set[self::EMAIL];
                $this->first_name = $this->set[self::FNAME];
                $this->last_name = $this->set[self::LNAME];
            } else {
                $this->level = 0;
            }
        }
        return $this->level;
    }

    /**
     * [login description]
     * @author Marian Munteanu
     * @date   2022-12-27T21:14:23+0200
     */
    function login($usr, $pass): bool {
        if (($uid = $this->getUserID($usr, $pass)) > 0) {
            $this->app->setClientSettings([self::XAT => $this->set[self::XAT]]);
            $this->app->hd_rs[$this->app->tokenHeader()] = $this->set[self::XAT];
            return true;
        }
        return false;
    }

    /**
     * [getUserID description]
     * @author Marian Munteanu
     * @date   2023-01-05T12:30:14+0200
     */
    function getUserID($usr, $pass): int {
        return $this->load(0, [self::USERNAME . ' =' => $usr, self::PASS . ' =' => $this->dbPass($pass),]);
    }

    /**
     * [dbPass description]
     * @author Marian Munteanu
     * @date   2023-01-04T14:42:23+0200
     */
    function dbPass($pass) {
        return md5($pass . ($this->uPassSec ?? null));
        //return password_hash($pass, PASSWORD_DEFAULT);
    }

    /**
     * [logout description]
     * @author Marian Munteanu
     * @date   2022-12-31T12:46:22+0200
     */
    function logout(): bool {
        $this->app->setClientSettings([self::XAT => null]);
        return true;
    }

    /**
     * [isLogged description]
     * @author Marian Munteanu
     * @date   2022-12-26T12:10:18+0200
     * @return [type]                   [description]
     */
    function isLogged() {
        return $this->getLevel() > 0;
    }

}
