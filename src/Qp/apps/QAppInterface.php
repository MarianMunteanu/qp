<?php

/**
* Marian Munteanu @ 2020
* 2020-04-09 00:25 marian (marian)
* QpAppInterface.php
*/

namespace Qp\apps;

use \Symfony\Component\HttpFoundation\Response;

interface QAppInterface {
    function onLoad();
    function getResponse(): Response;
}
