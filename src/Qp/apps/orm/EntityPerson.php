<?php

namespace Qp\apps\orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of EntityPerson
 *
 * @author Marian Jan 25, 2024 11:41:55 PM
 * @since Jan 25, 2024
 */
#[ORM\MappedSuperclass]
class EntityPerson extends EntityData {

    #[ORM\Column(type: "string", length: 64, unique: true)]
    public $EMAIL;

    #[ORM\Column(type: "string", length: 64)]
    public $NUME;

    #[ORM\Column(type: "string", length: 64)]
    public $PRENUME;

}
