<?php

namespace Qp\apps\orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Marian Jan 25, 2024 5:42:47 PM
 * @since Jan 25, 2024
 */
#[ORM\MappedSuperclass]
abstract class Entity extends \Qp\QObject {

    var $app = null;
    var $o = null;

    #[ORM\Id, ORM\Column(type: 'integer'), ORM\GeneratedValue]
    private int|null $ID;

    #[ORM\Column(type: "datetime", options: ["default" => "CURRENT_TIMESTAMP"])]
    private \DateTime $DATA_ADAUGARE;

    public function __construct(\Qp\apps\QApp $app, array $o = null) {
        $this->app = $app;
        $this->o = $o;
        $this->DATA_ADAUGARE = new \DateTime("now", new \DateTimeZone('Europe/Bucharest'));
    }

    /** @author Marian Munteanu @date 1/25/24, 6:43 PM */
    public function getName(): string {
        $lname = $this->name();
        if ($this->o && isset($this->o['name']) && $this->o['name']) {
            $lname = $this->o['name'];
        }
        return $this->app->getEntityPrefix() . $lname;
    }
    
    /** @author Marian Munteanu @date 1/27/24, 5:15 PM */
    public function save($extra = []): bool {
        $this->app->em->persist($this);
        $this->app->em->flush();
        return true;
    }

}
