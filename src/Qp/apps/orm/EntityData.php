<?php

namespace Qp\apps\orm;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Marian Jan 25, 2024 7:01:01 PM
 * @since Jan 25, 2024
 */
#[ORM\MappedSuperclass]
abstract class EntityData extends Entity {

    #[ORM\Column(type: "datetime", options: ["default" => "CURRENT_TIMESTAMP"])]
    private \DateTime $DATA_MODIFICARE;

    #[ORM\Column(type: 'integer')]
    private int $VERSIUNE;

    #[ORM\Column(type: "string", length: 2, options: ["fixed" => true])]
    private $STATUS = 'N';

    public function save($extra = []): bool {
        $this->DATA_MODIFICARE = new \DateTime("now", new \DateTimeZone('Europe/Bucharest'));
        if (isset($this->VERSIUNE)) {
            $this->STATUS = 'E';
        }
        $this->VERSIUNE = isset($this->VERSIUNE) ? $this->VERSIUNE + 1 : 1;
        if (!isset($extra['virtual'])) {
            $ts = $this->getAuditClass();
            eval($ts);
            $ad_e = (new ($this->getAuditName())($this->app));
            $ad_e->save(['virtual' => true]);
        }
        return parent::save($extra);
    }

    /** @author Marian Munteanu @date 1/27/24, 10:24 PM */
    public function getAuditName(){
        return 'h_'.$this->name();
    }
    /** @author Marian Munteanu @date 1/27/24, 10:33 PM */
    public function getAuditClass(){
        return '#[\Doctrine\ORM\Mapping\Entity]
            class '.$this->getAuditName().' extends '.$this::class.' {
                #[\Doctrine\ORM\Mapping\Column(type: "integer")] public int $ID_ELEMENT;
                public function save($extra = []): bool {return parent::save($extra);}
            } ';
    }

}
