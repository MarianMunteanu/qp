<?php

namespace Qp\apps\modules;

/**
 * Description of QAdmin
 * 
 * @Qp\QSettings(defaultLevel=500)
 *
 * @author Marian Jan 19, 2024 7:47 PM
 * @since Jan 19, 2024
 */
#[\Qp\QSettings(defaultLevel: 500)]
class QAdmin extends QModule {

    /** @Qp\QARoute("/") */
    #[\Qp\QARoute("/")]
    function onIndex() {
        echo "Admin module index";
    }

    /** @Qp\QARoute("/login", level=0) */
    #[\Qp\QARoute("/login", level: 0)]
    function onLogin() {
        $u = $this->app->getUser();
        if ($u->isLogged()) {
            echo "logat: ".$u->set['USERNAME']." [".$u->getLevel()."]  <form method='POST' action='" . $this->app->generateUrl([], 'Command') . "'>"
                    . "<input type='hidden' name='source' value='" . $this->app->generateUrl() . "'>"
                    . "<input type='hidden' name='WM_COMMAND' value='LOGOUT'>"
                    . "<input type='submit' value='Logout'></form>";
            return;
        }
        $f = [];
        $f['User'] = "text";
        $f['Pass'] = "password";
        echo "<form method='POST' action='" . $this->app->generateUrl([], 'Command') . "'>"
                . "\n<input type='hidden' name='source' value='" . $this->app->generateUrl() . "'>"
                . "\n<input type='hidden' name='WM_COMMAND' value='LOGIN'>"
                ;
        foreach ($f as $n => $t) {
            echo "\n<label for='$n'>$n:</label><br>"
                    . "<input type='$t' id='$n' name='$n' value=''><br>"
                    ;
        }
        echo '<br>'
        .($this->app->cnf()['useCaptcha']??false?'<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="g-recaptcha" data-sitekey="'.$this->app->cnf()['Captcha_siteKey'].'"></div>':'')
            . "<input type='submit' value='Submit'>"
            . "</form> ";
    }

}
