<?php

namespace Qp\apps\modules;

/**
 * Description of QModule
 *
 * @author Marian Jan 19, 2024 6:05:46 PM
 * @since Jan 19, 2024
 */
#[\AllowDynamicProperties]
abstract class QModule {

    const MODULES = ['admin' => "\\" . __NAMESPACE__ . "\\QAdmin"];

    public function __construct(\Qp\apps\QApp $app, array $o = null) {
        $this->app = $app;
        $this->o = $o;
    }

    public function call($a) {
        if ($this->app->verifyRoute()) {
            $this->{$a['met']}($a['attributes']);
        }
    }

}
