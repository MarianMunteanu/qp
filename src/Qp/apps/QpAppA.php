<?php

/**
* Marian Munteanu @ 2020
* 2020-04-01 17:54 marian (marian)
* AbstractQpApp.php
*/

namespace Qp\apps;

use \Qp\QObject;
use \Qp\QString;
use \Qp\QCoreApplication;

use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\Routing\RouteCollection;
use \Symfony\Component\Routing\Route;
use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\Routing\Matcher\UrlMatcher;
use \Symfony\Component\Routing\Generator\UrlGenerator;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
use \Symfony\Component\Routing\Exception\MethodNotAllowedException;

abstract class QpAppA extends QObject implements QpAppInterface {

    protected $coreApplication = null;
    protected $request = null;
    protected $response = null;
    protected $path = null;
    protected $rootCollection = null;
    protected $appCollection = null;
    protected $route = null;

    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-08T11:31:52+0200
     * @param  QCoreApplication         $app [description]
     */
    public function __construct(QCoreApplication $app) {
        ob_start();

        $this->setCoreApplication( $app );

        $this->request = $this->coreApplication->request;
        $this->path = $this->request->getPathInfo();
        $this->rootCollection = new RouteCollection();
        $this->appCollection = new RouteCollection();
        $this->context = new RequestContext();
        $this->context->fromRequest($this->request);

        $this->addRoute(['name'=>'Index', 'path'=>'/']);
        $this->addRoute(['name'=>'Command', 'path'=>'/command', 'methods'=>'POST']);

        $this->onLoad();

        foreach (get_class_methods($this) as $met) {
            $r_met = $met;
            if($met !== 'onLoad' && strpos($met, "on") === 0 && ($met=substr($met, 2))){
                // if already exists will not be added
                if (!($this->appCollection->get($met))) {
                    $ar = null;
                    try {
                        foreach ((new \Doctrine\Common\Annotations\SimpleAnnotationReader())->getMethodAnnotations(new \ReflectionMethod($this, $r_met)) as $o) {
                            // only the 1st route annotation
                            if ($o instanceof \Qp\QARoute) {
                                $ar = $o;
                                break;
                            }
                        }
                    } catch (\Exception $ex) {
                    }
                    $this->addRoute(['name' => $met, 'ARoute' => $ar]);
                }
            }
        }
        if($this->coreApplication->r_appName){
            $this->appCollection->addPrefix('/'.$this->coreApplication->r_appName);
        }
        $this->rootCollection->addCollection($this->appCollection);
        $this->matcher = new UrlMatcher($this->rootCollection, $this->context);
        $this->generator = new UrlGenerator($this->rootCollection, $this->context);
        try{
            $attributes = $this->matcher->match( $this->path );
            // if the route name was set with annotations
            $met = 'on' . (
                    ($tr = $this->appCollection->get($attributes["_route"])) 
                    && isset($tr->metoda) 
                    && ($tm = $tr->metoda) 
                    ? $tm : $attributes["_route"]
                    );
            if( method_exists( $this , $met ) ){
                $this->route = $attributes["_route"];
                $this->{$met}($attributes);
            }
        } catch (ResourceNotFoundException | MethodNotAllowedException $exception) {
            $this->response = $this->coreApplication->get404();
        }
    }

    /**
     * [onIndex description]
     * @author Marian Munteanu
     * @date   2020-04-09T16:20:14+0200
     * @return [type]                   [description]
     */
    public function onIndex(){
    }

    /**
     * [onCommand description]
     * @author Marian Munteanu
     * @date   2020-04-09T16:18:17+0200
     * @return [type]                   [description]
     */
    public function onCommand(){
    }

    /**
     * [getAppName description]
     * @author Marian Munteanu
     * @date   2020-04-02T00:41:33+0200
     */
    public function getAppName(): QString {
        return (new QString( $this->name() ))->trim(); //->toLowerCase();
    }

    /**
     * [setCoreApplication description]
     * @author Marian Munteanu
     * @date   2020-04-06T02:59:48+0200
     * @param  \Qp\QCoreApplication     $app [description]
     */
    public function setCoreApplication(QCoreApplication $app) {
        $this->coreApplication = $app;
    }

    /**
     * [getCoreApplication description]
     * @author Marian Munteanu
     * @date   2020-04-08T10:48:09+0200
     */
    public function getCoreApplication(): QCoreApplication {
        return $this->coreApplication;
    }

    public function cApp(): QCoreApplication {
        return $this->getCoreApplication();
    }

    /**
     * [appDir description]
     * @author Marian Munteanu
     * @date   2020-04-08T15:48:46+0200
     */
    public function appDir(){
        return dirname( (new \ReflectionClass($this))->getFilename() );
    }

    /**
     * [getResponse description]
     * @author Marian Munteanu
     * @date   2020-04-08T16:07:13+0200
     * @return [type]                   [description]
     */
    public function getResponse(): Response {
        $rsp = $this.'';
        @ob_end_clean();
        if($this->response){
            return $this->response;
        }
        return new Response( $rsp , Response::HTTP_OK );
    }

    public function __toString(){
        $prev = ob_get_clean();
        @ob_end_clean();
        return $prev;
    }

    /**
     * [addRoute description]
     * @author Marian Munteanu
     * @date   2020-04-09T23:00:54+0200
     * @param  [type]                   $o [description]
     */
    public function addRoute( $o ){
        if(!isset($o['name']) || !$o['name']){
            throw new \Exception("Missing route name", 1);
        }
        if($this->appCollection->get($o['name'])){
            return null;
        }
        if(!isset($o['path']) || !$o['path']){
            // throw new \Exception("Missing route path", 1);
            $o['path'] = '/'.strtolower($o['name']);
        }
        if (isset($o['ARoute']) && ($ar = $o['ARoute']) && $ar instanceof \Qp\QARoute) {
            $tr = new Route($ar->getPath(),
                    $ar->getDefaults(),
                    $ar->getRequirements(),
                    $ar->getOptions(),
                    $ar->getHost(),
                    $ar->getSchemes(),
                    $ar->getMethods(),
                    $ar->getCondition());
            $r_name = $o['name'];
            if (($tn = trim($ar->getName())) && strlen($tn) > 0) {
                $r_name = $tn;
                // if the name was set with annotations, we need to keep the method name
                $tr->metoda = $o['name'];
            }
            $this->appCollection->add($r_name, $tr);
        } else {
            $defaults = isset($o['defaults']) && ($t = $o['defaults']) ? $t : [];
            $requirements = isset($o['requirements']) && ($t = $o['requirements']) ? $t : [];
            $methods = isset($o['methods']) && ($t = $o['methods']) ? $t : [];
            $this->appCollection->add($o['name'], new Route($o['path'], $defaults, $requirements, [], '', [], $methods));
        }
    }

    public function generateUrl($o = [], $croute = null){
        $route = $croute ? $croute : $this->route;
        if(!$route){
            return "";
        }
        $url = "";
        try{
            $url = $this->generator->generate( $route, $o );
        } catch( \Exception $ex ){
            if( $this->cApp()->isDev() || $this->cApp()->isUat() ){
                $url = $ex->GetMessage();
            }
        }
        return $url;
    }

    /**
     * [dbSave description]
     * @author Marian Munteanu
     * @date   2020-04-17T12:16:43+0200
     * @param  string                   $name [description]
     * @param  array                    $vals [description]
     * @return [type]                         [description]
     */
    public function dbSave(string $name, array $vals){
        $e = new QpAppEntity($this,['name'=>$name]);
        $e->save($vals);
    }

    /**
     * [dbGet description]
     * @author Marian Munteanu
     * @date   2020-04-17T19:54:33+0200
     * @param  string                   $name [description]
     * @param  array                    $opt  [description]
     * @return [type]                         [description]
     */
    public function dbGet(string $name, array $opt): array {
        $e = new QpAppEntity($this,['name'=>$name]);
        return $e->get($opt);
    }

    /**
     * [view description]
     * @author Marian Munteanu
     * @date   2020-04-17T16:09:34+0200
     * @param  array                    $vars  [description]
     * @param  [type]                   $tname [description]
     * @return [type]                          [description]
     */
    protected function view(array $vars, $tname = null): string {
        if(!$tname){
            $tname = debug_backtrace()[1]['function'];
        }
        $tname = strtolower($tname);
        if(strpos($tname, "on") === 0){
            $tname = substr($tname, 2);
        }
        if(!$tname){
            throw new \Exception("Missing view name", 1);
        }
        if (!(isset(pathinfo($tname)['extension']))) {
            $tname = $tname . '.html';
        }
        $tpath = $this->appDir() . '/views/' . $tname; 
        if(!file_exists($tpath)){
            throw new \Exception("Missing view file", 1);
        }
        $loader = new \Twig\Loader\FilesystemLoader($this->appDir() . '/views');
        $twig = new \Twig\Environment($loader);
        return $twig->render($tname, ['app'=>$this] + $vars);
    }
    
    /**
     * [isDev description]
     * @author Marian Munteanu
     * @date   2022-02-26T23:37:36+0200
     * @return boolean                  [description]
     */
    public function isDev(){
        return $this->cApp()->isDev();
    }
}
