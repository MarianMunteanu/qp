<?php

/**
* Marian Munteanu @ 2020
* 2020-04-01 17:54 marian (marian)
* AbstractQpApp.php
*/

namespace Qp\apps;

use \Qp\QObject;
use \Qp\QString;
use \Qp\QCoreApplication;

use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\Routing\RouteCollection;
use \Symfony\Component\Routing\Route;
use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\Routing\Matcher\UrlMatcher;
use \Symfony\Component\Routing\Generator\UrlGenerator;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
use \Symfony\Component\Routing\Exception\MethodNotAllowedException;

abstract class QApp extends QObject implements QAppInterface {

    protected $coreApplication = null;
    protected $request = null;
    protected $response = null;
    protected $path = null;
    protected $rootCollection = null;
    protected $appCollection = null;
    protected $cookies = [];
    protected $context = null;
    protected $matcher = null;
    protected $generator = null;

    protected static $eventManager = null;
    protected static $config = null;

    public $route = null;
    public $hd_rq = [];
    public $hd_rs = [];

    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-08T11:31:52+0200
     * @param  QCoreApplication         $app [description]
     */
    public function __construct(QCoreApplication $app) {
        ob_start();

        $this->setCoreApplication( $app );

        $this->request = $this->coreApplication->request;
        $this->hd_rq = $this->request->headers;
        $this->path = $this->request->getPathInfo();
        $this->rootCollection = new RouteCollection();
        $this->appCollection = new RouteCollection();
        $this->context = new RequestContext();
        $this->context->fromRequest($this->request);

        $this->addRoute(['name'=>'Index', 'path'=>'/']);
        $this->addRoute(['name'=>'Command', 'path'=>'/command', 'methods'=>'POST']);

        $this->onLoad();
        
        $this->loadRoutes();
    }

    /**
     * [loadRoutes description]
     * @author Marian Munteanu
     * @date   2022-12-27T13:11:14+0200
     */
    protected function loadRoutes() {
        $this->parseRoutes($this, $this->appCollection,
                (($t_el = $this->getClassSettings() ?? false) ? ['settings' => $t_el->getArguments()] : []));

        if (($mdls = $this->cnf()['modules'] ?? false) && is_array($mdls)) {
            foreach ($mdls as $mdlK => $mdlAl) {
                if (array_key_exists($mdlK, \Qp\apps\modules\QModule::MODULES) 
                        && class_exists($mdlCls = \Qp\apps\modules\QModule::MODULES[$mdlK])
                ) {
                    if (!$mdlAl) {
                        $mdlAl = $mdlK;
                    }
                    //$settings = (new \Doctrine\Common\Annotations\SimpleAnnotationReader())->getClassAnnotation(new \ReflectionClass($mdlCls), \Qp\QSettings::class);
                    $settings = ($t_el = $this->getClassSettings($mdlCls) ?? false) ? $t_el->getArguments() : null;
                    $mdlColl = new RouteCollection();
                    $this->parseRoutes($mdlCls, $mdlColl, ['settings' => $settings]);
                    $mdlColl->addPrefix('/' . $mdlAl);
                    $mdlColl->addNamePrefix($mdlK . '-');
                    $this->appCollection->addCollection($mdlColl);
                }
            }
        }

        if ($this->coreApplication->r_appName) {
            $this->appCollection->addPrefix('/' . $this->coreApplication->r_appName);
        }
        $this->rootCollection->addCollection($this->appCollection);
        $this->matcher = new UrlMatcher($this->rootCollection, $this->context);
        $this->generator = new UrlGenerator($this->rootCollection, $this->context);
    }

    /** @author Marian Munteanu @date 1/20/24, 12:41 AM */
    protected function parseRoutes($obj, $collection, $e = []) {
        foreach (get_class_methods($obj) as $met) {
            $r_met = $met;
            if ($met = $this->isValidMethod($met)) {
                // if already exists will not be added
                if (!($collection->get($met))) {
                    $ar = null;
                    try {
                        //foreach ((new \Doctrine\Common\Annotations\SimpleAnnotationReader())->getMethodAnnotations(new \ReflectionMethod($obj, $r_met)) as $o) {
                        if ($o = ($t_el = ((new \ReflectionMethod($obj, $r_met))->getAttributes(\Qp\QARoute::class))[0] ?? null) ? $t_el->newInstance() : null) {
                            // only the 1st route annotation
                            if ($o instanceof \Qp\QARoute) {
                                $ar = $o;
                                //break;
                            }
                        }
                    } catch (\Exception $ex) {
                        
                    }
                    $this->addRoute(['name' => $met] + ($ar ? ['ARoute' => $ar] : []) 
                        + ['obj' => $obj] + ($e['settings'] ?? false ? ['settings' => $e['settings']] : []), $collection);
                }
            }
        }
    }

    /** @author Marian Munteanu @date 3/2/24, 10:36 PM */
    function getClassSettings($cls = null) {
        return ((new \ReflectionClass($cls ? $cls : $this))->getAttributes(\Qp\QSettings::class)[0] ?? null);
    }

    /** @author Marian Munteanu @date 1/19/24, 8:22 PM */
    protected function isValidMethod($met) {
        if ($met !== 'onLoad' && strpos($met, "on") === 0 && ($met = substr($met, 2))) {
            return $met;
        }
        return false;
    }

    /**
     * [matchRoute description]
     * @author Marian Munteanu
     * @date   2022-12-27T13:12:10+0200
     */
    protected function matchRoute(){
        try{
            $attributes = $this->matcher->match( $this->path );
            // if the route name was set with annotations
            $tr = $this->appCollection->get($attributes["_route"]);
            $met = 'on' . (
                    ($tr) && isset($tr->metoda) && ($tm = $tr->metoda) 
                    ? $tm : $attributes["_route"]
                    );
            $cObj = null;
            if (($cls = $tr->objClass ?? false) && is_string($cls) && (new QString($cls))->isValid()
                    && ($objMethod = $tr->objMethod ?? false) && is_string($objMethod) && (new QString($objMethod))->isValid()
                    && class_exists($cls) && method_exists($cls, ($met = 'on' . $objMethod))
            ) {
                $cObj = (new $cls($this));
            }
            if (method_exists(($cObj ? $cObj : ($cObj = $this)), $met)) {
                $this->route = $attributes["_route"];
                $cObj->call(['met'=>$met, 'attributes'=>$attributes]);
            }
        } catch (ResourceNotFoundException | MethodNotAllowedException $exception) {
            $this->response = $this->coreApplication->get404();
        }
    }

    /**
     * [addRoute description]
     * @author Marian Munteanu
     * @date   2020-04-09T23:00:54+0200
     * @param  [type]                   $o [description]
     */
    public function addRoute( $o, $col = null ){
        if (!$col) {
            $col = $this->appCollection;
        }
        if(!isset($o['name']) || !$o['name']){
            throw new \Exception("Missing route name", 1);
        }
        if ($col->get($o['name'])) {
            return null;
        }
        if(!isset($o['path']) || !$o['path']){
            // throw new \Exception("Missing route path", 1);
            $o['path'] = '/'.strtolower($o['name']);
        }
        $tr = null;
        $r_name = $o['name'];
        if (isset($o['ARoute']) && ($ar = $o['ARoute']) && $ar instanceof \Qp\QARoute) {
            $tr = new \Qp\QRoute($ar->getPath(),
                    $ar->getDefaults(),
                    $ar->getRequirements(),
                    $ar->getOptions(),
                    $ar->getHost(),
                    $ar->getSchemes(),
                    $ar->getMethods(),
                    $ar->getCondition());
            if (($tn = trim($ar->getName()??'')) && strlen($tn) > 0) {
                $r_name = $tn;
                // if the name was set with annotations, we need to keep the method name
                $tr->metoda = $o['name'];
            }
            $tr->level = $ar->getLevel();
        } else {
            $defaults = isset($o['defaults']) && ($t = $o['defaults']) ? $t : [];
            $requirements = isset($o['requirements']) && ($t = $o['requirements']) ? $t : [];
            $methods = isset($o['methods']) && ($t = $o['methods']) ? $t : [];
            $tr = new \Qp\QRoute($o['path'], $defaults, $requirements, [], '', [], $methods);
        }
        if (($obj = $o['obj'] ?? false) && is_string($obj) && (new QString($obj))->isValid()) {
            $tr->objClass = $obj;
            $tr->objMethod = $o['name'];
        }
        if (!isset($tr->level) && ($sett = ($o['settings'] ?? false)) && ($sett['defaultLevel'] ?? false)) {
            $tr->level = $sett['defaultLevel'];
        }
        $col->add($r_name, $tr);
    }

    /**
     * [getResponse description]
     * @author Marian Munteanu
     * @date   2020-04-08T16:07:13+0200
     * @return [type]                   [description]
     */
    public function getResponse(): Response {
        $this->matchRoute();
        $rsp = $this . '';
        @ob_end_clean();
        $r = ($tr = $this->response) ? $tr : new Response($rsp, Response::HTTP_OK);
        foreach ($this->cookies as $k => $v) {
            $r->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie($k, $v));
        }
        foreach ($this->hd_rs as $k => $v) {
            $r->headers->set($k, $v);
        }
        return $r;
    }

    /**
     * [view description]
     * @author Marian Munteanu
     * @date   2020-04-17T16:09:34+0200
     * @param  array                    $vars  [description]
     * @param  [type]                   $tname [description]
     * @return [type]                          [description]
     */
    protected function view(array $vars, $tname = null): string {
        if (!$tname) {
            $tname = debug_backtrace()[1]['function'];
        }
        $tname = strtolower($tname);
        if (strpos($tname, "on") === 0) {
            $tname = substr($tname, 2);
        }
        if (!$tname) {
            throw new \Exception("Missing view name", 1);
        }
        if (!(isset(pathinfo($tname)['extension']))) {
            $tname = $tname . '.html';
        }
        $tpath = $this->appDir() . '/views/' . $tname;
        if (!file_exists($tpath)) {
            throw new \Exception("Missing view file", 1);
        }
        $loader = new \Twig\Loader\FilesystemLoader($this->appDir() . '/views');
        $twig = new \Twig\Environment($loader);
        return $twig->render($tname, ['app' => $this] + $vars);
    }

    public function generateUrl($o = [], $croute = null, int $refType = UrlGenerator::ABSOLUTE_PATH) {
        $route = $croute ? $croute : $this->route;
        if (!$route) {
            return "";
        }
        $url = "";
        try {
            $url = $this->generator->generate($route, $o, $refType);
        } catch (\Exception $ex) {
            if ($this->cApp()->isDev() || $this->cApp()->isUat()) {
                $url = $ex->GetMessage();
            }
        }
        return $url;
    }

    /**
     * [dbSave description]
     * @author Marian Munteanu
     * @date   2020-04-17T12:16:43+0200
     * @param  string                   $name [description]
     * @param  array                    $vals [description]
     * @return [type]                         [description]
     */
    public function dbSave(string $name, array $vals){
        $e = new QAppEntity($this,['name'=>$name]);
        $e->save($vals);
    }

    /**
     * [dbGet description]
     * @author Marian Munteanu
     * @date   2020-04-17T19:54:33+0200
     * @param  string                   $name [description]
     * @param  array                    $opt  [description]
     * @return [type]                         [description]
     */
    public function dbGet(string $name, array $opt): array {
        $e = new QAppEntity($this,['name'=>$name]);
        return $e->get($opt);
    }

    protected function call($a) {
        if ($this->verifyRoute()) {
            $this->{$a['met']}($a['attributes']);
        }
    }

    /** @author Marian Munteanu @date 1/20/24, 10:45 AM */
    public function verifyRoute($route = null) {
        return true;
    }

    /**
     * [onLoad description]
     * @author Marian Munteanu
     * @date   2022-07-02T12:21:14+0200
     * @return [type]                   [description]
     */
    public function onLoad(){
    }

    /**
     * [onIndex description]
     * @author Marian Munteanu
     * @date   2020-04-09T16:20:14+0200
     * @return [type]                   [description]
     */
    public function onIndex(){
    }

    /**
     * [onCommand description]
     * @author Marian Munteanu
     * @date   2020-04-09T16:18:17+0200
     * @return [type]                   [description]
     */
    public function onCommand(){
    }

    /** @author Marian Munteanu @date 1/21/24, 1:36 PM */
    public function parseCommand($cmds){
        if (($wm_cmd = $this->cApp()->request->request->get('WM_COMMAND')) && array_key_exists($wm_cmd, $cmds)) {
            $cmds[$wm_cmd]($this->cApp()->request->request);
        }
        $sr = $this->cApp()->request->request->all()['source'];
        $this->response = ($sr == "no_redirect" ?? false) ? new \Symfony\Component\HttpFoundation\Response()
                : new \Symfony\Component\HttpFoundation\RedirectResponse($sr ? $sr : "/");
        $this->setForApi();
        return;
    }

    /** @author Marian Munteanu @date 2/25/24, 6:55 PM */
    function setForApi(){
        //$this->response->headers->set('Access-Control-Allow-Credentials', 'true');
        if (($org = $this->hd_rq->get('Origin')) && ($orgs = $this->config('origins')) && is_array($orgs)
                && in_array($org, $orgs)) {
            $this->response->headers->set('Access-Control-Allow-Origin', $org);
        }
        $this->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, PATCH');
        $this->response->headers->set('Access-Control-Allow-Headers', $this->tokenHeader());
    }

    /** @author Marian Munteanu @date 3/10/24, 3:38 PM */
    function tokenHeader() {
        return 'x-' . $this->getAppName()->toLowerCase() . '-tok';
    }

    /** @author Marian Munteanu @date 3/10/24, 5:36 PM */
    function to($route, $o = []) {
        $this->response = new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl($o, $route));
    }

    /** @author Marian Munteanu @date 3/10/24, 6:10 PM */
    function handleCod(int $cod, $route): Response|null {
        return null;
    }

    /**
     * [getAppName description]
     * @author Marian Munteanu
     * @date   2020-04-02T00:41:33+0200
     */
    public function getAppName(): QString {
        return (new QString( $this->name() ))->trim(); //->toLowerCase();
    }

    /** @author Marian Munteanu @date 1/25/24, 9:51 PM */
    public function getEntityPrefix(): string {
        return $this->cApp()->getAppName() . "_" . $this->getAppName() . "_";
    }

    /** @author Marian Munteanu @date 1/26/24, 12:16 AM */
    public static function getEventManager(QApp $app) {
        if (!self::$eventManager) {
            self::$eventManager = new \Doctrine\Common\EventManager;
            self::$eventManager->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, new class($app) {

                var $app = null;
                var $prefix = null;

                public function __construct($o) {
                    $this->app = $o;
                }

                public function loadClassMetadata(\Doctrine\ORM\Event\LoadClassMetadataEventArgs $eventArgs) {
                    $this->prefix = $this->app->getEntityPrefix();
                    $classMetadata = $eventArgs->getClassMetadata();
                    if (!$classMetadata->isInheritanceTypeSingleTable() || $classMetadata->getName() === $classMetadata->rootEntityName) {
                        $classMetadata->setPrimaryTable([
                            'name' => $this->prefix . $classMetadata->getTableName()
                        ]);
                    }

                    foreach ($classMetadata->getAssociationMappings() as $fieldName => $mapping) {
                        if ($mapping['type'] == \Doctrine\ORM\Mapping\ClassMetadata::MANY_TO_MANY && $mapping['isOwningSide']) {
                            $mappedTableName = $mapping['joinTable']['name'];
                            $classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->prefix . $mappedTableName;
                        }
                    }
                }
            });
        }
        return self::$eventManager;
    }

    /**
     * [setCoreApplication description]
     * @author Marian Munteanu
     * @date   2020-04-06T02:59:48+0200
     * @param  \Qp\QCoreApplication     $app [description]
     */
    public function setCoreApplication(QCoreApplication $app) {
        $this->coreApplication = $app;
    }

    /**
     * [getCoreApplication description]
     * @author Marian Munteanu
     * @date   2020-04-08T10:48:09+0200
     */
    public function getCoreApplication(): QCoreApplication {
        return $this->coreApplication;
    }

    public function cApp(): QCoreApplication {
        return $this->getCoreApplication();
    }

    /**
     * [appDir description]
     * @author Marian Munteanu
     * @date   2020-04-08T15:48:46+0200
     */
    public function appDir(){
        return dirname( (new \ReflectionClass($this))->getFilename() );
    }

    /**
     * [dataDir description]
     * @author Marian Munteanu
     * @date   2023-01-03T10:24:27+0200
     */
    public function dataDir($file = null) {
        return $this->appDir() . '/data' . ($file ? '/' . $file : '');
    }
    
    /**
     * @author Marian Munteanu
     * @date   2023-12-26T22:31:36+0200
     */
    public function getStaticDirApp() {
        return $this->appDir() . '/static';
    }

    /**
     * @author Marian Munteanu
     * @date   2023-12-26T22:50:36+0200
     */
    public function getStaticDirWeb() {
        return $this->cApp()->o[QCoreApplication::DIR_WEB_ROOT] . "/static/" . $this->getAppName();
    }

    /**
     * @author Marian Munteanu
     * @date   2023-12-26T22:12:36+0200
     */
    public function getStaticUrl() {
        return "/static/" . $this->getAppName();
    }

    /**
     * [isDev description]
     * @author Marian Munteanu
     * @date   2022-02-26T23:37:36+0200
     * @return boolean                  [description]
     */
    public function isDev() {
        return $this->cApp()->isDev();
    }
    
    /** @author Marian Munteanu @date 12/17/23, 9:05 PM */
    function validateCryptoData() {
        if (!isset($this->crypto)) {
            $this->crypto = $this->cnf()['crypto'];
        }
        if (!(isset($this->crypto) && is_array($t_arr = $this->crypto) && (function () use ($t_arr) {
                    foreach (['pass', 'cifru', 'iv'] as $k) {
                        if (!array_key_exists($k, $t_arr)) return false;
                    } return true; })() )) {
            throw new \Exception("Missing crypto array in app");
        }
    }

    /** @author Marian Munteanu @date 12/17/23, 8:52 PM */
    function crypt($s) {
        $this->validateCryptoData();
        return base64_encode(openssl_encrypt($s, $this->crypto['cifru'], $this->crypto['pass'], 0, $this->crypto['iv']));
    }

    /** @author Marian Munteanu @date 12/17/23, 9:13 PM */
    function decrypt($s) {
        $this->validateCryptoData();
        return openssl_decrypt(base64_decode($s), $this->crypto['cifru'], $this->crypto['pass'], 0, $this->crypto['iv']);
    }

    /** @author Marian Munteanu @date 12/17/23, 10:27 PM */
    function getClientSettings(): array|null {
        $cc = $this->request->cookies->get($this->cApp()->getAppName() . '_' . $this->getAppName());
        if(!$cc){
            return null;
        }
        $dec = json_decode($this->decrypt($cc), true);
        $r = is_array($s = $dec[$this->getAppName() . ''] ?? null) ? $s : [];
        return $r;
    }

    /** @author Marian Munteanu @date 12/17/23, 10:29 PM */
    function setClientSettings(array $s) {
        $set = [];
        $set[$this->getAppName() . ''] = [];
        if (($se = $this->getClientSettings() ?? null) && is_array($se)) {
            $set[$this->getAppName() . ''] = $se;
        }
        foreach ($s as $k => $v) {
            $set[$this->getAppName() . ''][$k] = $v;
        }
        $this->cookies[$this->cApp()->getAppName() . '_' . $this->getAppName()] = $this->crypt(json_encode($set, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
    }

    /** @author Marian Munteanu @date 12/20/23, 11:03 AM */
    public function cnf() {
        if (self::$config) {
            return self::$config;
        }
        $cf_a = file_exists($cf = $this->appDir() . '/etc/config.php') ? include $cf : [];
        $cf_e = file_exists($ce = $this->appDir() . '/etc/config_' . (new QString($this->cApp()->env))->trim()->toLowerCase() . '.php') ? include $ce : [];
        self::$config = array_merge($this->cnf ?? [], array_merge($cf_a, $cf_e));
        return self::$config;
    }

    /** @author Marian Munteanu @date 1/31/24, 10:53 PM */
    public function config(string $path) {
        $path = new QString($path);
        $cnf = $this->cnf();
        if (!$path->isValid() || !$cnf) {
            return null;
        }
        $te = null;
        foreach ($path->split("/") as $c) {
            $te = (!$te && isset($cnf[$c])) ? $cnf[$c] : (isset($te[$c]) ? $te[$c] : null);
            if (!$te) {
                return null;
            }
        }
        return $te;
    }

    public function __toString() {
        $prev = ob_get_clean();
        @ob_end_clean();
        return is_string($prev) ? $prev : '';
    }

}
