<?php

/**
* Marian Munteanu @ 2022
* 2022-12-17 17:17 marian (marian)
* QpAppSec.php
*/

namespace Qp\apps;

abstract class QAppSec extends QApp {

    protected $users = [];
    protected $user = null;

    const LEVEL_ADMIN = 100;

    function onCommand($o = null) {
        $cmds = [
            'LOGIN' => function ($r) {
                if ($this->cnf()['useCaptcha'] ?? false) {
                }
                if (($usr = $r->get('User')) && ($pss = $r->get('Pass'))) {
                    $this->getUser()->login($usr, $pss);
                }
            },
            'LOGOUT' => function ($r) {
                $this->getUser()->logout();
            }
        ];
        return $this->parseCommand($cmds);
    }

    public function verifyRoute($route = null) {
        if (!$route) {
            $route = $this->route;
        }
        $rt = $this->appCollection->get($route);
        $protected = (isset($rt->level) && ($routeLevel = (int) $rt->level) > 0);
        if ($protected) {
            $userLevel = $this->getUser()->getLevel();
            $cod = ($userLevel > 0) ? (($routeLevel > $userLevel) ? 403 : 0) : 401;
            if ($cod > 0) {
                $this->response = ($hr = $this->handleCod($cod, $route)) ? $hr : new \Symfony\Component\HttpFoundation\Response("", $cod);
                return false;
            }
        }
        return true;
    }

    /**
     * [getUser description]
     * @author Marian Munteanu
     * @date   2023-01-05T10:27:23+0200
     */
    public function getUser(): User {
        if (null === $this->user) {
            $this->user = new User($this);
            $this->user->uPassSec = isset($this->uPassSec) ? $this->uPassSec : $this->cnf()['uPassSec'];
        }
        return $this->user;
    }

}
