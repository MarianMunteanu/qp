<?php

/**
* Marian Munteanu @ 2020
* 2020-04-05 23:48 marian (marian)
* QObject.php
*/

namespace Qp;

abstract class QObject {

	/**
	 * [name description]
	 * @author Marian Munteanu
	 * @date   2020-04-16T18:38:10+0200
	 * @return [type]                   [description]
	 */
	public function name(): string {
		return (new \ReflectionClass($this))->getShortName();
	}
}
