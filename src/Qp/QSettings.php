<?php

namespace Qp;

/**
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 *
 * @author Marian Jan 20, 2024 12:11:26 PM
 * @since Jan 20, 2024
 */
#[\Attribute]
class QSettings {

    public int $defaultLevel = 0;

}
