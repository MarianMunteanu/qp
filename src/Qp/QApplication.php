<?php

/**
* Marian Munteanu @ 2020
* 2020-04-05 23:24 marian (marian)
* QApplication.php
*/

namespace Qp;

abstract class QApplication extends QCoreApplication {

    /**
     * [initApp description]
     * @author Marian Munteanu
     * @date   2020-04-05T23:30:04+0200
     * @return [type]                   [description]
     */
    public function initApp() {
    }
}
