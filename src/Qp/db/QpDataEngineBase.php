<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 19:11 marian (marian)
* QpDataEngineBase.php
*/

namespace Qp\db;

use \Qp\QObject;

abstract class QpDataEngineBase extends QObject implements DatabaseEngineInterface {

    /**
     * [getAutoIncrement description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:16:20+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function getAutoIncrement(Entity $e): int {
        return $e->getLaslId() + 1;
    }

    /**
     * [getName description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:16:31+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function getName(Entity $e): string {
        return $e->getName().'';
    }
    
    /**
     * [getEM description]
     * @author Marian Munteanu
     * @date   2022-12-27T21:17:23+0200
     */
    function getEM(){
        return null;
    }
    
    /**
     * [wr description]
     * @author Marian Munteanu
     * @date   2022-12-27T21:18:12+0200
     */
    function wr(array $wr) {
        $wherenq = [];
        foreach ($wr as $key => $value) {
            $ls = strrpos($key, ' ');
            $rk = trim(mb_substr($key, 0, $ls));
            $sg = trim(mb_substr($key, $ls));
            if ($rk && $sg) {
                $wherenq[] = [$rk, $sg, $value];
            }
        }
        return $wherenq;
    }

}
