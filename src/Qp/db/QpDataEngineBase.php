<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 19:11 marian (marian)
* QpDataEngineBase.php
*/

namespace Qp\db;

use \Qp\QObject;

abstract class QpDataEngineBase extends QObject implements DatabaseEngineInterface {

    /**
     * [getAutoIncrement description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:16:20+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function getAutoIncrement(Entity $e): int {
        return $e->getLaslId() + 1;
    }

    /**
     * [getName description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:16:31+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function getName(Entity $e): string {
        return $e->getName().'';
    }

}
