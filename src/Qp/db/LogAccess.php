<?php

/**
* Marian Munteanu @ 2020
* 2020-04-17 12:55 marian (marian)
* LogAccess.php
*/

namespace Qp\db;

use \Qp\QCoreApplication;

class LogAccess extends Entity {
    public function __construct(QCoreApplication $app) {
        parent::__construct($app);
        
        $this->save($this->cApp->logArr);
    }
}
