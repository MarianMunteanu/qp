<?php

/**
* Marian Munteanu @ 2020
* 2020-04-17 12:55 marian (marian)
* LogAccess.php
*/

namespace Qp\db;

use \Qp\QCoreApplication;

/** @\Doctrine\ORM\Mapping\Entity **/
class LogAccess extends Entity {
    
    /** @Column(type="string", nullable=true) **/ public $IP;
    /** @Column(type="string", nullable=true) **/ public $UserAgent;
    /** @Column(type="string", nullable=true) **/ public $RequestUri;
    /** @Column(type="string", nullable=true) **/ public $Referer;
    /** @Column(type="string", nullable=true) **/ public $Tara;
    /** @Column(type="string", nullable=true) **/ public $Oras;
    
    public function __construct(QCoreApplication $app) {
        parent::__construct($app);
        
        $this->save($this->cApp->logArr);
    }
}
