<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 19:07 marian (marian)
* QpDataEngineDummy.php
*/

namespace Qp\db;

class QpDataEngineDummy extends QpDataEngineBase {
    
    public $td = null;
    var $f = null;
    var $cApp = null;

    public function __construct( array $o ){
        $this->f = fopen('php://stderr', 'w');
        $this->cApp = $o['cApp'];
    }

    /**
     * [save description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:27:01+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function save(Entity $e): bool{
        $this->err("save() on ".$this->name().": ");
        $this->err("Tbl: ".$e->getName());
        $this->err("Val: ".print_r($e->el,true));
        return true;
    }

    /**
     * [err description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:28:08+0200
     * @param  [type]                   $m [description]
     * @return [type]                      [description]
     */
    private function err($m) {
        fwrite($this->f, "[" . \Qp\QString::now("d H:i:s") . "] " . $m . "\n");
    }

    function getLaslId(Entity $e): int {
        return 10;
    }

    function get(Entity $e, array $options): array {
        //debug_print_backtrace();die;
        //var_dump(debug_backtrace());die;
        if (NULL === $this->td && isset($this->cApp->loadedApp)) {
            $this->td = include $this->cApp->loadedApp->appDir() . '/data/test_db_data.php';
        }
        $this->err("get() on ".$this->name().": ");
        $this->err("Tbl: ".$e->getName());
        $ta = explode('_', $e->getName());
        $tn = end($ta);
        $this->err("tn: $tn");
        $this->err("options: ".print_r($options,true));
        $wr = $this->wr($options['wr'] ?? []);
        $this->err("wr: ".print_r($wr,true));
        if ($this->td && array_key_exists($tn, $this->td)) {
            $r = null;
            if(empty($wr)){
                $this->err("fara conditii, return all");
                $this->err(print_r($this->td[$tn], true));
                return $this->td[$tn];
            }
            foreach ($wr as $v) {
                if (false !== ($tk = array_search($v[2], array_column($this->td[$tn], $v[0])))) {
                    //var_dump($tk);die;
                    $el = $this->td[$tn][$tk];
                    $this->err("rezultat: tk=[".print_r($tk,1)."] array_search($v[2], array_column(this->td[$tn], $v[0]))" . print_r($el, true));
                    if(null==$r){
                        $r=$el;
                    }
                    return [$el];
                }
            }
            $this->err("rezultat: null");
        }
        return [];
        //return [['ID' => 8, 'a' => 'b', 'c' => 'd'], ['a' => 'b2', 'c' => 'd2']];
    }

}
