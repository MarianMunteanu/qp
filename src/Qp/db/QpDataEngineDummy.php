<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 19:07 marian (marian)
* QpDataEngineDummy.php
*/

namespace Qp\db;

class QpDataEngineDummy extends QpDataEngineBase {

    public function __construct( array $o ){
        $this->f = fopen('php://stderr', 'w');
    }

    /**
     * [save description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:27:01+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    function save(Entity $e): bool{
        $this->err("save() on ".$this->name().": ");
        $this->err("Tbl: ".$e->getName());
        $this->err("Val: ".print_r($e->el,true));
        return true;
    }

    /**
     * [err description]
     * @author Marian Munteanu
     * @date   2020-04-16T19:28:08+0200
     * @param  [type]                   $m [description]
     * @return [type]                      [description]
     */
    private function err($m){
        fwrite($this->f, $m."\n");
    }

    function getLaslId(Entity $e): int {
        return 10;
    }

    function get(Entity $e, array $options): array {
        return [['a'=>'b','c'=>'d'],['a'=>'b2','c'=>'d2']];
    }

}
