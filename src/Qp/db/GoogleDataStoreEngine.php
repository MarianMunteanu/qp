<?php

/**
* Marian Munteanu @ 2020
* 2020-04-17 02:26 marian (marian)
* GoogleDataStoreEngine.php
*/

namespace Qp\db;

use \Google\Cloud\Datastore\DatastoreClient;

class GoogleDataStoreEngine extends QpDataEngineBase
{
    var $datastore = null;
    
    private $allowedOrders = [
        'ASC' => \Google\Cloud\Datastore\Query\Query::ORDER_ASCENDING,
        'DESC' => \Google\Cloud\Datastore\Query\Query::ORDER_DESCENDING
    ];
    
    public function __construct( array $o ){
        $this->datastore = new DatastoreClient( [ 'projectId' => $o['projectId'] ] );
    }
    
    /**
     * [save description]
     * @author Marian Munteanu
     * @date   2020-04-17T02:30:00+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    public function save(Entity $e): bool {
        $tn = $e->getName();
        $el = $this->datastore->entity(($k = $e->key ?? false) ? $this->datastore->key($tn, $k) : $tn);
        foreach ($e->el as $c => $v) {
            $el[$c] = $v;
        }
        $r = $this->datastore->upsert($el);
        return ($r) ? true : false;
    }

    /**
     * [get description]
     * @author Marian Munteanu
     * @date   2020-04-17T20:00:57+0200
     * @param  Entity                   $e       [description]
     * @param  array                    $options [description]
     * @return [type]                            [description]
     */
    public function get(Entity $e, array $options): array {
        $limit = (int)(isset($options['limit']) ? $options['limit'] : 10);

        $what = isset($options['what']) && is_array($options['what']) ? implode(', ', $options['what']) : "*";

        $wherenq = [];

        $ws = '';
        $bindings = [];
        if(isset($options['wr'])){
            $options['where'] = $options['wr'];
        }
        if(isset($options['where']) && is_array($options['where'])){
            $and = [];
            foreach ($options['where'] as $key => $value) {
                $ls = strrpos($key, ' ');
                $rk = trim(mb_substr($key, 0, $ls));
                $sg = trim(mb_substr($key, $ls));
                if($rk && $sg){
                    $and[] = sprintf("%s %s @%s", $rk, $sg, $rk);
                    $bindings[$rk] = $value;
                    $wherenq[] = [$rk, $sg, $value];
                }
            }
            if(count($and)>0){
                $ws = ' WHERE '.implode(' AND ', $and);
            }
        }

        $query = $this->datastore->query()->kind($e->getName());
        foreach ($wherenq as $k => $v) {
            $query->filter($v[0], $v[1], $v[2]);
        }

        if(isset($options['ORD']) && is_array($options['ORD']) && ($ord = $options['ORD'])){
            foreach ($ord as $k=>$v){
                $query->order($k, $this->allowedOrders[$v]);
            }
        }

        $query->limit($limit);

        $ret = [];
        $result = $this->datastore->runQuery($query);
        foreach($result as $el){
            $ret[] = $el;
        }
        return $ret;
    }

    /**
     * [getLaslId description]
     * @author Marian Munteanu
     * @date   2020-04-17T02:30:11+0200
     * @param  Entity                   $e [description]
     * @return [type]                      [description]
     */
    public function getLaslId(Entity $e): int {
        $query = $this->datastore->query()->kind($e->getName())->order(Entity::ID, \Google\Cloud\Datastore\Query\Query::ORDER_DESCENDING)->limit(1);
        $result = $this->datastore->runQuery($query);
        foreach($result as $el){
            if( ($id = (int)@$el[Entity::ID])>0 ){
                return $id;
            }
        }
        return 0;
    }
}
