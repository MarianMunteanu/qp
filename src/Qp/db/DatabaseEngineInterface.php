<?php

/**
* Marian Munteanu @ 2020
* 2020-04-01 02:04 marian (marian)
* DatabaseEngineInterface.php
*/

namespace Qp\db;

interface DatabaseEngineInterface {

    function save(Entity $e): bool;

    function get(Entity $e, array $options): array;

    function getLaslId(Entity $e): int;

    function getAutoIncrement(Entity $e): int;

    function getName(Entity $e): string;
}
