<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 14:47 marian (marian)
* Entity.php
*/

namespace Qp\db;

use \Qp\QObject;
use \Qp\QString;
use \Qp\QCoreApplication;

/** @\Doctrine\ORM\Mapping\MappedSuperclass **/
#[\AllowDynamicProperties]
abstract class Entity extends QObject {
    const ID = 'ID';
    const DATA_ORA = 'DATA_ORA';
    
    /** @Id @Column(type="integer") @GeneratedValue * */ public $ID;
    /** @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"}) */ public $DATA_ORA;

    protected $cApp = null;
    protected $dbEngine = null;

    var $col = [self::ID, self::DATA_ORA];
    var $set = [];
    var $el = [];

    public function __construct(QCoreApplication $app) {
        $this->cApp = $app;
        $this->dbEngine = $this->cApp->dbEngine;

        foreach ((new \ReflectionClass($this))->getConstants() as $t_col) {
            if (!in_array($t_col, $this->col, true)) {
                $this->col[] = $t_col;
            }
        }
    }

    /**
     * [getName description]
     * @author Marian Munteanu
     * @date   2020-04-16T15:26:06+0200
     * @return [type]                   [description]
     */
    public function getName(): string {
        return $this->cApp->getAppName() . "_" . $this->name();
    }

    /**
     * [save description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:45:55+0200
     * @param  array                    $extra [description]
     * @return [type]                          [description]
     */
    public function save($extra = []): bool {
        $this->el = [];
        foreach ($this->col as $c) {
            $this->el[$c] = @$this->set[$c];
        }
        foreach( $extra as $c => $v ){
            $this->el[$c] = $v;
        }
        $this->el[self::DATA_ORA] = new \DateTime("now", new \DateTimeZone('Europe/Bucharest'));
        //$this->DATA_ORA = new \DateTime("now", new \DateTimeZone('Europe/Bucharest'));
        if (!(((int) @$this->el[self::ID]) > 0)) {
            $this->el[self::ID] = $this->getAutoIncrement();
        }
        return $this->dbEngine->save($this);
    }

    /**
     * [get description]
     * @author Marian Munteanu
     * @date   2020-04-17T20:00:30+0200
     * @param  array                    $options [description]
     * @return [type]                            [description]
     */
    public function get(array $options): array {
        return $this->dbEngine->get($this, $options);
    }

    /**
     * [getAutoIncrement description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:47:12+0200
     * @return [type]                   [description]
     */
    public function getAutoIncrement(): int {
        return $this->getLaslId() + 1;
    }

    /**
     * [getLaslId description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:48:05+0200
     * @return [type]                   [description]
     */
    public function getLaslId(): int {
        return $this->dbEngine->getLaslId($this);
    }
    
    /**
     * [load description]
     * @author Marian Munteanu
     * @date   2022-12-18T12:18:05+0200
     * @return [type]                   [description]
     */
    public function load(int $id, array $wr = null): int {
        if (($id > 0 || $wr) && ($t_arr = $this->get(['wr' => $wr ? $wr : [self::ID. ' =' => $id], 'limit' => 1])) 
                && count($t_arr) > 0 && ($t_el = $t_arr[0]) 
                && isset($t_el[self::ID]) && ($t_id = (int) $t_el[self::ID]) > 0) {
            foreach ($this->col as $col) {
                $this->set[$col] = $t_el[$col] ?? null;
            }
            return $t_id;
        }
        return 0;
    }

}
