<?php

/**
* Marian Munteanu @ 2020
* 2020-04-16 14:47 marian (marian)
* Entity.php
*/

namespace Qp\db;

use \Qp\QObject;
use \Qp\QString;
use \Qp\QCoreApplication;

abstract class Entity extends QObject {
    const ID = 'ID';
    const DATA_ORA = 'DATA_ORA';

    protected $cApp = null;
    protected $dbEngine = null;

    var $col = [self::ID, self::DATA_ORA];
    var $set = [];

    public function __construct(QCoreApplication $app) {
        $this->cApp = $app;
        $this->dbEngine = $this->cApp->dbEngine;

        foreach ((new \ReflectionClass($this))->getConstants() as $t_col) {
            if (!in_array($t_col, $this->col, true)) {
                $this->col[] = $t_col;
            }
        }
    }

    /**
     * [getName description]
     * @author Marian Munteanu
     * @date   2020-04-16T15:26:06+0200
     * @return [type]                   [description]
     */
    public function getName(): string {
        return $this->cApp->getAppName() . "_" . $this->name();
    }

    /**
     * [save description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:45:55+0200
     * @param  array                    $extra [description]
     * @return [type]                          [description]
     */
    public function save($extra = []): bool {
        $this->el = [];
        foreach ($this->col as $c) {
            $this->el[$c] = @$this->set[$c];
        }
        foreach( $extra as $c => $v ){
            $this->el[$c] = $v;
        }
        $this->el[self::DATA_ORA] = new \DateTime("now", new \DateTimeZone('Europe/Bucharest'));
        if (!(((int) @$this->el[self::ID]) > 0)) {
            $this->el[self::ID] = $this->getAutoIncrement();
        }
        return $this->dbEngine->save($this);
    }

    /**
     * [get description]
     * @author Marian Munteanu
     * @date   2020-04-17T20:00:30+0200
     * @param  array                    $options [description]
     * @return [type]                            [description]
     */
    public function get(array $options): array {
        return $this->dbEngine->get($this, $options);
    }

    /**
     * [getAutoIncrement description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:47:12+0200
     * @return [type]                   [description]
     */
    public function getAutoIncrement(): int {
        return $this->getLaslId() + 1;
    }

    /**
     * [getLaslId description]
     * @author Marian Munteanu
     * @date   2020-04-17T01:48:05+0200
     * @return [type]                   [description]
     */
    public function getLaslId(): int {
        return $this->dbEngine->getLaslId($this);
    }
}
