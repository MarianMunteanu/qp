<?php

/**
* Marian Munteanu @ 2022
* 2022-02-27 01:14 marian (marian)
* QWord.php
*/

namespace Qp;

class QWord extends QObject {

    function __construct($filename, $data) {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $data);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="' . $filename . '.doc"');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }

}
