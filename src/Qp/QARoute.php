<?php

/**
* Marian Munteanu @ 2022
* 2022-02-27 15:13 marian (marian)
* QARoute.php
*/

namespace Qp;

/**
 * Annotation class for QRoute().
 *
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 *
 * @author 2022-02-27 15:17 marian (marian)
 */
#[\Attribute]
class QARoute extends \Symfony\Component\Routing\Annotation\Route {

    protected $level = null;

    public function __construct(
            string|array|null $path = null,
            private ?string $name = null,
            private array $requirements = [],
            private array $options = [],
            private array $defaults = [],
            private ?string $host = null,
            array|string $methods = [],
            array|string $schemes = [],
            private ?string $condition = null,
            private ?int $priority = null,
            ?string $locale = null,
            ?string $format = null,
            ?bool $utf8 = null,
            ?bool $stateless = null,
            private ?string $env = null
            , ?int $level = null
    ) {
        parent::__construct($path, $name, $requirements, $options, $defaults, $host, $methods, $schemes, $condition, $priority, $locale, $format, $utf8, $stateless, $env);
        $this->setLevel($level);
    }

    /**
     * @author Marian Munteanu
     * @date   2022-12-17T17:36:21+0200
     * @param type $level
     */
    public function setLevel($level) {
        $this->level = $level;
    }

    /**
     * @author Marian Munteanu
     * @date   2022-12-17T17:37:28+0200
     * @return type
     */
    public function getLevel() {
        return $this->level;
    }

}
