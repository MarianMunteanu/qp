<?php

/**
* Marian Munteanu @ 2022
* 2022-02-27 15:13 marian (marian)
* QARoute.php
*/

namespace Qp;

/**
 * Annotation class for QRoute().
 *
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 *
 * @author 2022-02-27 15:17 marian (marian)
 */
class QARoute extends \Symfony\Component\Routing\Annotation\Route {
}
