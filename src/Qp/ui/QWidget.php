<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 02:20 marian (marian)
* QWidget.php
*/

namespace Qp\ui;

use \Qp\QObject;

class QWidget extends QObject {
    const BOOTSTRAP = ['<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">' => ''];

    protected $parent = null;
    protected $widgets = [];
    protected $css = [];
    
    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:32:56+0200
     * @param  QWidget                  $parent [description]
     */
    public function __construct(QWidget $parent = null) {
        if($parent){
            $this->setParent($parent);
        }
    }

    /**
     * [setParent description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:48:03+0200
     * @param  QWidget                  $parent [description]
     */
    public function setParent(QWidget $parent){
        $this->parent = $parent;
    }

    /**
     * [add description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:35:39+0200
     * @param  QWidget                  $child [description]
     */
    public function add(QWidget $child) {
        $child->setParent($this);
        $this->widgets[] = $child;
    }

    /*function getTemplateFile() {
        return $this->classDir() . "/" . $this->name() . ".html";
    }*/

    function getCss(): array {
        $t_dep = [];
        foreach ($this->widgets as $w) {
            $t_dep += $w->getCss();
        }
        return $this->css + $t_dep;
    }

    public function __toString(){
        $ret = '';
        foreach ($this->widgets as $widget) {
            $ret .= $widget.'';
        }
        return $ret;
    }
}
