<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 02:20 marian (marian)
* QWidget.php
*/

namespace Qp\ui;

use \Qp\QObject;

class QWidget extends QObject {

    protected $parent = null;
    protected $widgets = [];
    
    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:32:56+0200
     * @param  QWidget                  $parent [description]
     */
    public function __construct(QWidget $parent = null) {
        if($parent){
            $this->setParent($parent);
        }
    }

    /**
     * [setParent description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:48:03+0200
     * @param  QWidget                  $parent [description]
     */
    public function setParent(QWidget $parent){
        $this->parent = $parent;
    }

    /**
     * [add description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:35:39+0200
     * @param  QWidget                  $child [description]
     */
    public function add(QWidget $child) {
        $child->setParent($this);
        $this->widgets[] = $child;
    }

    public function __toString(){
        $ret = '';
        foreach ($this->widgets as $widget) {
            $ret .= $widget.'';
        }
        return $ret;
    }
}
