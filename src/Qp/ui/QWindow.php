<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 01:59 marian (marian)
* QWindow.php
*/

namespace Qp\ui;

class QWindow extends QWidget {

    protected $app;

    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:12:20+0200
     * @param  AbstractQpApp            $app [description]
     */
    public function __construct(\Qp\apps\QApp $app) {
        $this->app = $app;
        parent::__construct(\null);
    }

    public function __toString(){
        $ret = '';

        /*if (is_file($tf = ($ar = $this->app->appDir() . '/templates/' . ($nm = $this->name() . '.tpl.php'))) 
                || is_file($tf = ($ar = ($ca = $this->app->cApp())->o[$ca::DIR_APP_ROOT] . '/src/templates/' . $nm))
                //|| is_file($tf = $this->getTemplateFile())
                || is_file($tf = ($this->classDir()) . '/' . $nm)
        ) {
            //$twig = new \Twig\Environment(new \Twig\Loader\FilesystemLoader(dirname($tf)));
            //$ret .= $twig->render(basename($tf), ['TITLE' => $this->app->getAppName(), 'WIDGETS' => $this->widgets, 'DEP' => $this->getDependencies()]);
            $ret .= include $tf;
            return $ret;
        }*/

        $body = "";
        foreach ($this->widgets as $widget) {
            $body .= $widget."\n<br>\n";
        }
        $ret = "<!DOCTYPE html>\n"
                . "<html>\n"
                . "<head>"
                . "<title>".$this->app->getAppName()."</title>\n"
                . implode("\n", array_keys($this->getCss()))
                . "</head>\n"
                ;
        $ret .= "<body>\n".$body."<body>\n";
        $ret .= "</html>\n";
        return $ret;
    }
}
