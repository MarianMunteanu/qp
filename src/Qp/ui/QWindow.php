<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 01:59 marian (marian)
* QWindow.php
*/

namespace Qp\ui;

class QWindow extends QWidget {

    protected $app;

    /**
     * [__construct description]
     * @author Marian Munteanu
     * @date   2020-04-12T02:12:20+0200
     * @param  AbstractQpApp            $app [description]
     */
    public function __construct(\Qp\apps\QpAppA $app) {
        $this->app = $app;
        parent::__construct(\null);
    }

    public function __toString(){
        $ret = "<!DOCTYPE html>\n<html>\n<head><title>".$this->app->getAppName()."</title></head>\n<body>\n";
        foreach ($this->widgets as $widget) {
            $ret .= $widget."\n<br>\n";
        }
        $ret .= "\n</body>\n</html>\n";
        return $ret;
    }
}
