<?php

/**
* Marian Munteanu @ 2023
* 2023-01-07 18:25 marian (marian)
* QTable.php
*/

namespace Qp\ui\widgets;

class QTable extends \Qp\ui\QWidget {

    public function __construct(array $cols, array $data, $o = []) {
        $this->cols = $cols;
        $this->data = $data;
        $this->o = $o;
        
        $this->css += self::BOOTSTRAP;
    }

    public function __toString() {
        $ret = '
            <table class="table table-striped table-hover">
            <thead>
            <tr class="d-flex">
            ';
        foreach ($this->cols as $k => $c){
            $ret .= '
                    <th scope="col" class="col-2">'.htmlspecialchars($k).'</th>'."\n";
        }
        $ret .= '
            </tr>
            </thead>
            <tbody>
            ';
        foreach ($this->data as $r){
            $ret .= '
                <tr class="d-flex">
                ';
            foreach ($this->cols as $k => $c){
                $ret .= '
                    <td class="col-2">'.htmlspecialchars(is_callable($c)?$c($r[$k]):$r[$k]).'</td>
                    ';
            }
            $ret .= '
                </tr>
                ';
        }
        $ret .= '
            </tbody>
            </table>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                  <li class="page-item">
                    <a class="page-link" href="' . ($this->o['prev'] ?? null) . '" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  ';
        foreach ($this->o['pages'] ?? [] as $k => $v) {
            $ret .= '
                  <li class="page-item"><a class="page-link" href="'.$v.'">'.$k.'</a></li>
                  ';
        }
        $ret .= '
                  <li class="page-item">
                    <a class="page-link" href="' . ($this->o['next'] ?? null) . '" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            ';
        return $ret;
    }

}
