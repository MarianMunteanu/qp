<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 02:51 marian (marian)
* QLabel.php
*/

namespace Qp\ui\widgets;

use \Qp\ui\QWidget;

class QLabel extends QWidget {
    public function __construct(string $text, $o = []) {
    	$this->o = $o;
        $this->text = $text;
    }

    public function __toString(){
    	if(isset($this->o['html']) && $this->o['html']){
    		return $this->text;
    	}
        return htmlspecialchars($this->text);
    }
}
