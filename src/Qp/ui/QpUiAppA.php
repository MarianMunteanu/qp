<?php

/**
* Marian Munteanu @ 2020
* 2020-04-12 12:43 marian (marian)
* QpUiAppA.php
*/

namespace Qp\ui;

use \Qp\QCoreApplication;
use \Qp\apps\QpAppA;
use \Qp\ui\QWindow;

abstract class QpUiAppA extends QpAppA {
    protected $mainWindow = null;
    abstract function build();

    public function __construct(QCoreApplication $app) {
        $this->mainWindow = new QWindow($this);
        parent::__construct($app);
    }

    /**
     * [add description]
     * @author Marian Munteanu
     * @date   2020-04-12T13:23:33+0200
     * @param  QWidget                  $child [description]
     */
    public function add(QWidget $child) {
        $this->mainWindow->add($child);
    }

    /**
     * [onIndex description]
     * @author Marian Munteanu
     * @date   2020-04-12T12:59:46+0200
     * @return [type]                   [description]
     */
    public function onIndex(){
        $this->build();
    }

    public function __toString(){
        return $this->mainWindow.'';
    }
}
