<?php

namespace Qp;

/**
 * Description of QApplicationGae
 *
 * @author Marian 1/25/24, 12:17 PM
 * @since Jan 25, 2024
 */
abstract class QApplicationGae extends QApplication {
    public function __construct($o) {
        if (!isset($o['projectId'])) {
            $o['projectId'] = getenv('GOOGLE_CLOUD_PROJECT');
        }
        if (!isset($o['env']) && isset($o['envs']) && is_array(($envs = $o['envs']))) {
            $o['env'] = in_array($o['projectId'], ['local', 'None'], true) ? 'DEV' : null;
            foreach ($envs as $n_env => $a_env) {
                if (in_array($o['projectId'], $a_env, true)) {
                    $o['env'] = $n_env;
                }
            }
        }
        parent::__construct($o);
    }

    public function initApp() {
        if ($this->isUat() || $this->isProd()) {
            $this->dbEngine = new \Qp\db\GoogleDataStoreEngine(['projectId' => $this->o['projectId']]);
        } else {
            $this->dbEngine = new \Qp\db\QpDataEngineDummy(['cApp' => $this]);
        }
        if ($this->o['LogActive'] ?? false) {
            new \Qp\db\LogAccess($this);
        }
    }

}
