<?php

/**
* Marian Munteanu @ 2022
* 2022-02-27 01:05 marian (marian)
* QPdf.php
*/

namespace Qp;

class QPdf extends QObject {

    function __construct($filename, $data) {
        $pdfOptions = new \Dompdf\Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new \Dompdf\Dompdf($pdfOptions);
        $dompdf->loadHtml($data);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . ".pdf", ["Attachment" => false]);
    }

}
