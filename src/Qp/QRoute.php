<?php

namespace Qp;

/**
 * Description of QRoute
 *
 * @author Marian Feb 1, 2024 10:14:19 PM
 * @since Feb 1, 2024
 */
#[\AllowDynamicProperties]
class QRoute extends \Symfony\Component\Routing\Route {
}
