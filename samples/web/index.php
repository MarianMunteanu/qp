<?php

require_once __DIR__ . '/../vendor/autoload.php';

$env = null;
$projectId = getenv('GOOGLE_CLOUD_PROJECT');

$env = in_array($projectId, ['local', 'None'], true) ? 'dev' : $env;

if (!$projectId && file_exists(__DIR__ . '/../local')) {
    $projectId = 'local';
}

class MyApp extends \Qp\QCoreApplication {
    public function initApp() {
    }
}

$o = [
    MyApp::DIR_WEB_ROOT => __DIR__, 
    MyApp::DIR_APP_ROOT => __DIR__ . '/..', 
    'projectId' => $projectId, 
    'LogActive' => true, 
    'env' => $env
];

$app = new MyApp( $o );
$app->exec();
