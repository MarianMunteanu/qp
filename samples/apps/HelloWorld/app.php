<?php

/**
* Marian Munteanu @ 2020
* 2020-04-06 02:15 marian (marian)
* app.php
*/

namespace apps;

class HelloWorld extends \Qp\apps\AbstractQpApp {
    function onLoad() {
    }
    
    function onIndex() {
        echo "Hello World!";
    }
}
